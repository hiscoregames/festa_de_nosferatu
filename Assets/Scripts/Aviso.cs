﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Aviso : MonoBehaviour
{
    public GameObject aviso;
    public GameObject telaCheia;
    public GameObject canvas;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Avisar(string textoAviso)
    {
        aviso.SetActive(true);
        aviso.GetComponent<Text>().text = textoAviso;
        Interacao.OnInteraction += FechaTelacheia;
        telaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia = true;
        canvas.GetComponent<Estatua>().estatua = true;
    }

       
    public void FechaTelacheia()
    {
        if (telaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia == true)
        {
            Interacao.OnInteraction -= FechaTelacheia;

            telaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia = false;
            canvas.GetComponent<Estatua>().estatua = false;
            aviso.SetActive(false);
            


        }
    }

   
}
