﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cadeado : MonoBehaviour
{
    public bool aberto;
    
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
            { anim.SetBool("Aberto", true); }
    }

    public void sumir()
    {
        StartCoroutine(ExampleCoroutine(3));
        gameObject.SetActive(false);
    }

    public void Abrir()
    {
       
            aberto = true;
            anim.SetBool("Aberto", true);
           
           



    }

    IEnumerator ExampleCoroutine(int tempo)
    {

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(tempo);



    }
}
