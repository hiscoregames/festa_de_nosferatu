﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo : MonoBehaviour
{
    public bool DemoMode=false;
    public float secsGamePlay;

    // Update is called once per frame
    void Update()
    {
        if (DemoMode == true)
        {
            StartCoroutine(cronometroFinal());
        }
    }

    IEnumerator cronometroFinal()
    {
        yield return new WaitForSeconds(secsGamePlay);
        //chama tela de agradecimento
    }
}
