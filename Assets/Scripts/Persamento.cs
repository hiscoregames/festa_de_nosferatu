﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Persamento : MonoBehaviour
{
    public Sprite Perseguido01, Perseguido02, Perseguido03, Perseguido04, Perseguido05, Perseguido06;
    public GameObject player;
    private Animator anim;
    // Update is called once per frame

    public void Start()
    {
        anim=GetComponent<Animator>();
    }

    public void cabecaVazia()
    {
        gameObject.SetActive(false);
        anim.SetBool("Perseguindo01", false);
        anim.SetBool("Perseguindo02", false);
        anim.SetBool("Perseguindo03", false);
        anim.SetBool("Perseguindo04", false);
        anim.SetBool("Perseguindo05", false);
        anim.SetBool("Perseguindo06", false);
    }

    public void Pensar(string Pensamento)
    {
        //Debug.Log(Pensamento);
        if (Pensamento == "Perseguindo01" && player.GetComponent<Movimento>().atual!=1)
        {
                anim.SetBool("Perseguindo01", true);
                StartCoroutine(TempoPensamento(10));
        }
            else if (Pensamento == "Perseguindo02" && player.GetComponent<Movimento>().atual != 2)
            {
                anim.SetBool("Perseguindo02", true);
                StartCoroutine(TempoPensamento(10));
            }

                else if (Pensamento == "Perseguindo03" && player.GetComponent<Movimento>().atual != 3)
                {
                    anim.SetBool("Perseguindo03", true);
                    StartCoroutine(TempoPensamento(10));
                }

                    else if (Pensamento == "Perseguindo04" && player.GetComponent<Movimento>().atual != 4)
                    {
                        anim.SetBool("Perseguindo04", true);
                        StartCoroutine(TempoPensamento(10));
                    }

                        else if (Pensamento == "Perseguindo05" && player.GetComponent<Movimento>().atual != 5)
                        {
                            anim.SetBool("Perseguindo05", true);
                            StartCoroutine(TempoPensamento(10));
                        }

                        else if (Pensamento == "Perseguindo06" && player.GetComponent<Movimento>().atual != 6)
                        {
                            anim.SetBool("Perseguindo06", true);
                            StartCoroutine(TempoPensamento(10));
                        }
        else
        {
            cabecaVazia();
        }
    }

    IEnumerator TempoPensamento(float tempo)
    {

        yield return new WaitForSeconds(tempo);
        cabecaVazia();
    }
}
