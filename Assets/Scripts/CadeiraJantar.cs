﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CadeiraJantar : MonoBehaviour
{
    Animator anim;
    public delegate void PushAction(string cor);
    public static event PushAction OnPush;
    public delegate void PullAction();
    public static event PullAction OnPull;
    bool pushed;
    public string cor;
    public bool interactable;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Acao()
    {
        if (interactable)
        {
            anim.SetTrigger("Interação");
            pushed = !pushed;
            if (pushed)
                OnPush(cor);
            else
                OnPull();
        }
    }

    void OnEnable()
    {
        Interacao.OnInteraction += Acao;
        PuzzleCadeiras1.OnFail += Resetar;
    }

    void OnDisable()
    {
        Interacao.OnInteraction -= Acao;
        PuzzleCadeiras1.OnFail -= Resetar;
    }

    void Resetar()
    {
        anim.SetTrigger("Interação");
        pushed = false;
    }
}