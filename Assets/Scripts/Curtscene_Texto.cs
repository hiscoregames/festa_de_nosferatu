﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curtscene_Texto : MonoBehaviour
{
    public GameObject[] imagens;
    private int next=0;
    // Start is called before the first frame update
   
    public void Reproduzir()
    {
        if (next <= imagens.Length)
        {
            imagens[next].SetActive(true);
            next++;
        }
        

    }

    public void Proximo()
    {
        if (next >= imagens.Length)
        {
            GetComponent<Convite_Abrir>().chamaGame();
        }
        else
        {
            Reproduzir();
        }
        
    }
}
