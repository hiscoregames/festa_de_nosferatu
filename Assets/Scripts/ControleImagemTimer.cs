﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControleImagemTimer : MonoBehaviour
{

    public GameObject ImagemDia;
    public GameObject ImagemNoite;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.GetComponent<Tempo>().TempoNoite == true)
        {
            ImagemDia.SetActive(false);
            ImagemNoite.SetActive(true);


        }

        if (gameObject.GetComponent<Tempo>().TempoNoite == false)
        {
            ImagemDia.SetActive(true);
            ImagemNoite.SetActive(false);


        }
    }
}
