﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visualizar_Tela_Cheia : MonoBehaviour
{
    public Sprite imagem;
    public GameObject modoTelaCheia;
    public bool intermediario;

    public GameObject player;

    public GameObject som;
    

    public void ChamaTela()
    {
        if(player.GetComponent<Movimento>().ativo==true)
        modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().VerImagem(imagem);
        som.GetComponent<Gerente_Som>().TocaSom(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (intermediario == false)
            {
                other.gameObject.GetComponent<Marcador_Player>().interacao = true;
                player = other.gameObject;
                Interacao.OnInteraction += ChamaTela;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (intermediario == false)
            {
                other.gameObject.GetComponent<Marcador_Player>().interacao = false;
                player = null;
                Interacao.OnInteraction -= ChamaTela;
            }
        }
    }
}
