﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCadeiras1 : MonoBehaviour
{
    List<string> cadeiras = new List<string>();
    int i = 0;
    public delegate void CheckAction();
    //public static event CheckAction OnSucess;
    public static event CheckAction OnFail;

    public GameObject bauChave;
    public GameObject som;

    void OnEnable()
    {
        CadeiraJantar.OnPush += Push;
        CadeiraJantar.OnPull += Pull;
    }

    void OnDisable()
    {
        CadeiraJantar.OnPush -= Push;
        CadeiraJantar.OnPull -= Pull;
    }

    void Push(string cor)
    {
        som.GetComponent<Gerente_Som>().TocaSom(0);

        cadeiras.Add(cor);
        foreach (var item in cadeiras)
        {
            Debug.Log(item);
        }
        if (cadeiras.Count == 6)
            Checar();
    }

    void Pull()
    {
        som.GetComponent<Gerente_Som>().TocaSom(0);

        if (cadeiras.Count > 0)
            cadeiras.RemoveAt(cadeiras.Count - 1);
        foreach (var item in cadeiras)
        {
            Debug.Log(item);
            //Debug.Log("Arrastou?");
            
        }
    }

    void Checar()
    {
        if (cadeiras[0] == "Laranja" && cadeiras[1] == "Azul" && cadeiras[2] == "Rosa" && cadeiras[3] == "Vermelha" && cadeiras[4] == "Amarela" && cadeiras[5] == "Verde")
        {
            //OnSucess();
            Sucesso();
            Debug.Log("Success");
        }
        else
            StartCoroutine(Fail());
    }

    IEnumerator Fail()
    {

        yield return new WaitForSeconds(1);
        OnFail();
        cadeiras.Clear();
        cadeiras.TrimExcess();
        Debug.Log("Fail");

        som.GetComponent<Gerente_Som>().TocaSom(0);
    }

    public void Sucesso()
    {
        bauChave.SetActive(true);
        bauChave.GetComponent<Bau>().RevelarMesa();
    }
    
}
