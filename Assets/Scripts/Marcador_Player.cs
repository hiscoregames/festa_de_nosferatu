﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Marcador_Player : MonoBehaviour
{
    public Vector3 pos; 
    public GameObject marcador;
    public Sprite MarcadorAtivo;
    public Sprite MarcadorImg;

    public bool interacao;
    public GameObject e;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Morto>().morto == false)
        {
            marcador.transform.position = Camera.main.WorldToScreenPoint(transform.position + pos);
        }
        else
        {
            marcador.SetActive(false);
        }
        
        //marcador.transform.position = gameObject.transform.position + pos;

        if (gameObject.GetComponent<Movimento>().atual == gameObject.GetComponent<Movimento>().personagem && GetComponent<Morto>().morto==false)
        {

            if (interacao == false)
            {
                marcador.SetActive(true);
                e.SetActive(false);
                marcador.GetComponent<Image>().sprite = MarcadorAtivo;
            }
            else
            {
                marcador.SetActive(false);
                e.SetActive(true);

            }
            
        }
        else
        {
            marcador.GetComponent<Image>().sprite = MarcadorImg;
        }
    }
}
