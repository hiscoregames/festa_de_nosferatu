﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Convite_Abrir : MonoBehaviour
{
    public GameObject conviteAberto;
    public bool Aberto=false;
    public bool reproduziu = false;
    

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {

            if (Aberto == false)
            {
                conviteAberto.SetActive(true);
                Aberto = true;
                StartCoroutine(ChamaGame());
                

            }
            else if (Aberto == true && reproduziu==false)
            {
                GetComponent<Curtscene_Texto>().Reproduzir();
                reproduziu = true;
            }
                
        }
        
    }

  
    public void chamaGame()
    {
        Loading.LoadScene("Castelo");
        Debug.Log("Chama o Game");
    }

    IEnumerator ChamaGame()
    {
      
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(20);
        GetComponent<Curtscene_Texto>().Reproduzir();


    }
}
