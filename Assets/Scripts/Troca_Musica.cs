﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Troca_Musica : MonoBehaviour
{

    
    public AudioSource Trilha;
    public AudioClip MusicaDia, MusicaNoite;


    // Start is called before the first frame update
    public void Start()
    {
        //Trilha = GetComponent<AudioSource>();
    }
    // Update is called once per frame


    public void somNoite()
    {
        Trilha.clip = MusicaNoite;
        Trilha.Play();
    }

    public void somDia()
    {
        Trilha.clip = MusicaDia;
        Trilha.Play();
    }
}
