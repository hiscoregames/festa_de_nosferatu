﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objeto_Tela_Cheia : MonoBehaviour
{

    private Sprite ImagemTela;
    public GameObject Telacheia;
    public bool ModotelaCheia;
    public GameObject canvas;

    //public Sprite fundoPreto;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().sprite = ImagemTela;

       
    }

    public void FechaTelacheia()
    {
        if (ModotelaCheia == true)
        {
            Interacao.OnInteraction -= FechaTelacheia;

            ModotelaCheia = false;
            canvas.GetComponent<Estatua>().estatua = false;
            ImagemTela = null;
            Telacheia.SetActive(false);
            
            

        }
    }

    public void VerImagem(Sprite imagem)
    {
        Telacheia.SetActive(true);
        Interacao.OnInteraction += FechaTelacheia;
        ImagemTela = imagem;
        ModotelaCheia = true;
        canvas.GetComponent<Estatua>().estatua=true;
        

    }

   
    
}
