﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portao_Principal : MonoBehaviour
{
    private GameObject chave;
    public GameObject Cadeado01, Cadeado02, Cadeado03, Cadeado04, Cadeado05, Cadeado06;
    public Animator Tranca01, Tranca02, Tranca03, Tranca04, Tranca05, Tranca06;
    public bool vitoria=false;

    public GameObject MadeiraCima;
    public GameObject MadeiraBaixo;

    private GameObject player;
    private GameObject hand;

    public Sprite aviso;
    public GameObject modoTelaCheia;
    public GameObject som;


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            StartCoroutine(VitoriaGame());
        }

        if(Cadeado01.GetComponent<Cadeado>().aberto==true &&
            Cadeado02.GetComponent<Cadeado>().aberto == true &&
            Cadeado03.GetComponent<Cadeado>().aberto == true &&
            Cadeado04.GetComponent<Cadeado>().aberto == true &&
            Cadeado05.GetComponent<Cadeado>().aberto == true &&
            Cadeado06.GetComponent<Cadeado>().aberto == true)
        {
            StartCoroutine(VitoriaGame());
        }


        if(Cadeado01.GetComponent<Cadeado>().aberto &&
            Cadeado02.GetComponent<Cadeado>().aberto)
        {
            StartCoroutine(DerrubaMadeira(1));
            
        }

        if(Cadeado03.GetComponent<Cadeado>().aberto == true &&
            Cadeado04.GetComponent<Cadeado>().aberto == true && 
            Cadeado05.GetComponent<Cadeado>().aberto == true && 
            Cadeado06.GetComponent<Cadeado>().aberto == true)
        {
            StartCoroutine(DerrubaMadeira(2));
            
        }

    }

    public void Portao()
    {
        

        if (chave != null)
        {
            AbreCadeado(chave.GetComponent<Chave_Control>().NChave);
            
        }
        else
        {
            modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().VerImagem(aviso);
        }
    }

    public void AbreCadeado(int Nchave)
    {
        Destroy(chave);
        player.GetComponent<itemMao>().Largou();
        som.GetComponent<Gerente_Som>().TocaSom(0);

        if (Nchave == 1)
        {
            Debug.Log("chave recebida");

            Cadeado01.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca01.SetBool("Tranca01", true);

        }

        if (Nchave == 2)
        {
            Debug.Log("chave recebida");
            Cadeado02.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca02.SetBool("Tranca02", true);

        }

        if (Nchave == 3)
        {
            Debug.Log("chave recebida");
            Cadeado03.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca03.SetBool("Tranca03", true);

        }

        if (Nchave == 4)
        {
            Debug.Log("chave recebida");
            Cadeado04.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca04.SetBool("Tranca04", true);

        }

        if (Nchave == 5)
        {
            Debug.Log("chave recebida");
            Cadeado05.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca05.SetBool("Tranca05", true);

        }

        if (Nchave == 6)
        {
            Debug.Log("chave recebida");
            Cadeado06.GetComponent<Cadeado>().Abrir();
            //StartCoroutine(ExampleCoroutine(1));
            Tranca06.SetBool("Tranca06", true);

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject;
            other.gameObject.GetComponent<Marcador_Player>().interacao = true;
            Interacao.OnInteraction += Portao;

            foreach (Transform child in player.transform)
            {
                if (child.tag == "Hand")

                    hand = child.gameObject;
                foreach (Transform child2 in hand.transform)
                {
                    if (child2.tag == "Chave")

                        chave = child2.gameObject;
                    //Debug.Log(chave);

                }

            }


        }

        if(other.tag == "Chave")
        {
            chave=other.gameObject;
            //Debug.Log(other);
                
            
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {
            player = null;
            Interacao.OnInteraction -= Portao;
            other.gameObject.GetComponent<Marcador_Player>().interacao = false;

        }

        if (other.tag == "Chave")
        {
            chave = null;
        }
    }

    IEnumerator VitoriaGame()
    {

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(3);
        Loading.LoadScene("Vitoria");
               
    }
    IEnumerator DerrubaMadeira(int m)
    {
        yield return new WaitForSeconds(2);
        if (m == 1)
        {
            MadeiraCima.GetComponent<Animator>().SetBool("MadeiraCima", true);
        }
        else if (m == 2)
        {
            MadeiraBaixo.GetComponent<Animator>().SetBool("MadeiraBaixo", true);
        }
    }

}
