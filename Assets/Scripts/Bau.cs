﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bau : MonoBehaviour
{
    public bool aberto;
    public GameObject conteudo;
    private Animator anim;
    public Animator bau;
    public GameObject som;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
   
    public void Conteudo()
    {
        if(aberto == false)
        {

            anim.SetBool("Aberto", true);
            aberto = true;
            conteudo.SetActive(true);
            som.GetComponent<Gerente_Som>().TocaSom(0);
            //conteudo.transform.GetChild(0).SetParent(null);
        }

       /* else if (aberto == true && transform.childCount>0)
        {
            conteudo.transform.GetChild(0).SetParent(null);
        }*/

        /*else 
        {
            anim.SetBool("Aberto", false);
            aberto = false;
            
        }*/
    }

    public void RevelarMesa()
    {
        bau.SetBool("PuzzleJantar01", true);
        //anim.SetBool("Revelado", true);
        som.GetComponent<Gerente_Som>().TocaSom(1);

    }

   
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<Marcador_Player>().interacao = true;
            Interacao.OnInteraction += Conteudo;

        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {
            //Debug.Log(other);
            other.gameObject.GetComponent<Marcador_Player>().interacao = false;
            if (aberto == true)
            { 
                anim.SetBool("Aberto", false);
                aberto = false;
                StartCoroutine(espera());
                som.GetComponent<Gerente_Som>().TocaSom(0);

            }

            Interacao.OnInteraction -= Conteudo;

        }

    }

    IEnumerator espera()
    {
        yield return new WaitForSeconds(3);
        conteudo.SetActive(false);

    }

}
