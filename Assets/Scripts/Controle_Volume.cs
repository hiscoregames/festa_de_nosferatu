﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controle_Volume : MonoBehaviour
{
    float VolMas;
    float VolMus;
    float VolEfx;

    public GameObject Melodia;
    public GameObject som;

    public Slider sliderMaster, sliderEFX, sliderMus;

    public bool test;
    // Start is called before the first frame update
    void Start()
    {
       sliderMaster.value = PlayerPrefs.GetFloat("Master");
       sliderEFX.value = PlayerPrefs.GetFloat("EFX");
       sliderMus.value = PlayerPrefs.GetFloat("Musica");
        test = true;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void VolumeMaster(float vol)
    {
        VolMas = vol;
        AudioListener.volume = VolMas;
        PlayerPrefs.SetFloat("Master",VolMas);

        if (som != null && Melodia!=null && test==true)
        {
            Melodia.GetComponent<Gerente_Som>().TocaSom(0);
            som.GetComponent<Gerente_Som>().TocaSom(0);
        }
        
    }

    public void VolumeMusica(float vol)
    {
        VolMus = vol;
        GameObject[] Musicas = GameObject.FindGameObjectsWithTag("Musica");
       
        for (int i = 0; i < Musicas.Length; i++)
        {
            Musicas[i].GetComponent<AudioSource>().volume = VolMus;
            
        }
        PlayerPrefs.SetFloat("Musica", VolMus);

        if (som != null && Melodia != null && test == true)
        {
            Melodia.GetComponent<Gerente_Som>().TocaSom(0);
           
        }

    }

    public void VolumeEFX(float vol)
    {
        VolEfx= vol;
        GameObject[] Efx = GameObject.FindGameObjectsWithTag("Efx");

        for (int i = 0; i < Efx.Length; i++)
        {
            Efx[i].GetComponent<AudioSource>().volume = VolEfx;
        }
        PlayerPrefs.SetFloat("EFX", VolEfx);

        if (som != null && Melodia != null && test == true)
        {
            som.GetComponent<Gerente_Som>().TocaSom(0);

        }

    }
}
