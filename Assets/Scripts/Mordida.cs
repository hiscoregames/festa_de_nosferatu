﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mordida : MonoBehaviour
{
    //[SerializeField] Material red;
    Coroutine mordidoR;
    public GameObject Orlok;

    public bool morto = false;
    public Animator anim;
    public BoxCollider[] box;

    public GameObject canvas;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Orlok"))
            OrlokAI.OnBit += SerMordido;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Orlok"))
            OrlokAI.OnBit -= SerMordido;
    }

    void SerMordido()
    {
        if (mordidoR == null && GetComponent<Movimento>().perseguido==true)
            mordidoR = StartCoroutine(Mordido());
    }

    IEnumerator Mordido()
    {
        //GetComponent<MeshRenderer>().material = red;
        gameObject.GetComponent<Morto>().morto = true;
        canvas.GetComponent<Sobreviventes>().preenche(GetComponent<Movimento>().personagem);
        GetComponent<Movimento>().perseguido = false;
        

        //if (GetComponent<Movimento>() != null)
        //  Destroy(GetComponent<Movimento>());
        

        try
        {
            

            foreach (Transform child02 in transform)
            {
                if (child02.tag == "Hand")
                {
                    child02.GetComponent<BoxCollider>().enabled = false;
                    
                }

                foreach (Transform child in child02.transform)
                {
                    if (child.tag == "Chave" || child.tag == "Item")
                    {

                        box = child.GetComponents<BoxCollider>();
                        box[0].enabled = true;
                        box[1].enabled = true;
                    }

                }

            }
        }

        catch
        {

        }

        yield return new WaitForSeconds(0);
        Orlok.GetComponent<OrlokAI>().PegouPlayer();
        anim.SetBool("Morto", true);
        gameObject.tag = "Untagged";
        //gameObject.SetActive(false);
        //Destroy(gameObject);
        
    }
}
