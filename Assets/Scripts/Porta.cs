﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Porta : MonoBehaviour
{
    public bool Trancada;
    public Animator anim;
    public bool aberta;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    public void AbrirFecha()
    {
        if (Trancada == false)
        {

        
            if (aberta == false)
            {
            anim.SetBool("Aberta", true);
            aberta = true;
            }
        }

    }

    public void Tranca()
    {
        if (Trancada == true)
        {
            Trancada = false;
        }
        else
        {
            Trancada = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Interacao.OnInteraction += AbrirFecha;
            other.gameObject.GetComponent<Marcador_Player>().interacao = true;
        }

        if(other.tag == "Orlok")
        {
            AbrirFecha();
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player" || other.tag == "Orlok")
        {
            Debug.Log("Saiu");
            Interacao.OnInteraction -= AbrirFecha;
            anim.SetBool("Aberta", false);
            aberta = false;
            other.gameObject.GetComponent<Marcador_Player>().interacao = false;

        }
    }
}
