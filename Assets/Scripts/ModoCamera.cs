﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoCamera : MonoBehaviour
{
    public float minDistBorda;
    public float VelMove;

    public float posZ;
   
    public bool modoCamera=false;
    public GameObject filtro;
    public GameObject iconAtivo;
    public GameObject iconDesativo;
    public GameObject offset;

    public float limiteEsquerdaX;
    public float limiteDireitaX;
    public float limiteCimaZ;
    public float limiteBaixoZ;

    public GameObject canvas;

    public GameObject mouseInstrucao;
    private bool mouseApresentado=false;



    // Update is called once per frame
    void Update()
    {
        float telaAltura = Screen.height;
        float telaLargura = Screen.width;
        Vector2 mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        float yBorda = telaAltura - mouse.y;
        float xBorda = telaLargura - mouse.x;

        Vector3 mov = Vector3.zero;

        


        //if (canvas.GetComponent<Estatua>().estatua==false)
        //{
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (modoCamera == false && canvas.GetComponent<Estatua>().estatua==false)
                {
                    LigaCamera();

                }

                else if (modoCamera == true)
                {
                    DesligaCamera();
                }
            }
        //}


        if (modoCamera == true)
        {
            if (mouseApresentado == false)
            {
                mouseInstrucao.SetActive(true);
                mouseApresentado = true;
            }

            filtro.SetActive(true);
            iconAtivo.SetActive(true);
            iconDesativo.SetActive(false);

            if (yBorda < minDistBorda)
            {
                if (transform.position.z < limiteCimaZ)
                {
                    mov = new Vector3(mov.x, mov.y, minDistBorda - yBorda);
                }
            }
            else if (mouse.y < minDistBorda)
            {
                if (transform.position.z > limiteBaixoZ)
                {
                    mov = new Vector3(mov.x, mov.y, -(minDistBorda - mouse.y));
                }
            }

            if (xBorda < minDistBorda)
            {
                if (transform.position.x < limiteDireitaX)
                {
                    mov = new Vector3(minDistBorda - xBorda, mov.y, mov.z);
                }
            }
            else if (mouse.x < minDistBorda)
            {
                if (transform.position.x > limiteEsquerdaX)
                {
                    mov = new Vector3(-(minDistBorda - mouse.x), mov.y, mov.z);
                }
            }

            transform.Translate(mov * Time.deltaTime * VelMove, Space.World);

        }
    }

    public void DesligaCamera()
    {
        Debug.Log("Deligou Camera");
        modoCamera = false;
        canvas.GetComponent<Estatua>().estatua = false;
        filtro.SetActive(false);
        iconAtivo.SetActive(false);
        iconDesativo.SetActive(true);
        mouseInstrucao.SetActive(false);
        mouseApresentado = false;
        gameObject.transform.position = new Vector3(offset.transform.position.x, transform.position.y, offset.transform.position.z - 10);
    }

    public void LigaCamera()
    {
        modoCamera = true;
        canvas.GetComponent<Estatua>().estatua = true;
    }
}
