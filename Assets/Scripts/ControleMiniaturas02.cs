﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControleMiniaturas02 : MonoBehaviour
{
    public GameObject Player01;
    public GameObject Player02;
    public GameObject Player03;
    public GameObject Player04;
    public GameObject Player05;
    public GameObject Player06;

    public GameObject ImgAtualUI, ImgMinUI01, ImgMinUI02, ImgMinUI03, ImgMinUI04, ImgMinUI05, ImgMinUI06;

    public Sprite ImgAtual01;
    public Sprite ImgAtual02;
    public Sprite ImgAtual03;
    public Sprite ImgAtual04;
    public Sprite ImgAtual05;
    public Sprite ImgAtual06;

    public Sprite ImgAtualMorto01, ImgAtualMorto02;

    public Sprite ImgMin01;
    public Sprite ImgMin02;
    public Sprite ImgMin03;
    public Sprite ImgMin04;
    public Sprite ImgMin05;
    public Sprite ImgMin06;

    public Sprite ImgMinMorto01;
    public Sprite ImgMinMorto02;
    public Sprite ImgMinMorto03;
    public Sprite ImgMinMorto04;
    public Sprite ImgMinMorto05;
    public Sprite ImgMinMorto06;

    public Sprite ImgMinSelecionado01, ImgMinSelecionado02, ImgMinSelecionado03, ImgMinSelecionado04, ImgMinSelecionado05, ImgMinSelecionado06;

    public GameObject itemSelecionado, item01, item02, item03, item04, item05, item06;
   // public Sprite itemChave01, itemChave02, itemChave03, itemChave04, itemChave05, itemChave06, 
        //itemLivro01, itemLivro02, itemLivro03, itemLivro04, itemLivro05, itemLivro06;

    public GameObject offsetCamera;
    private int playerAtual;
    

    
    // Update is called once per frame
    void Update()
    {
        //Troca de miniaturas
        #region 

        if (Player01.GetComponent<Morto>().morto != true)   //se o player não foi destruido 
        {
            if (Player01.GetComponent<Movimento>().atual == 1 && Player01.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual01;
                ImgMinUI01.GetComponent<Image>().sprite = ImgMinSelecionado01;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI01.GetComponent<Image>().sprite = ImgMin01; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if (playerAtual == 1)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto01;
            }

            ImgMinUI01.GetComponent<Image>().sprite = ImgMinMorto01;

        }


        if (Player02.GetComponent<Morto>().morto !=true)   //se o player não foi destruido 
        {
            if (Player02.GetComponent<Movimento>().atual == 2 && Player01.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual02;
                ImgMinUI02.GetComponent<Image>().sprite = ImgMinSelecionado02;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI02.GetComponent<Image>().sprite = ImgMin02; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if( playerAtual == 2)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto02;
            }

            ImgMinUI02.GetComponent<Image>().sprite = ImgMinMorto02;

        }

        if (Player03.GetComponent<Morto>().morto != true)   //se o player não foi destruido 
        {
            if (Player03.GetComponent<Movimento>().atual == 3 && Player03.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual03;
                ImgMinUI03.GetComponent<Image>().sprite = ImgMinSelecionado03;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI03.GetComponent<Image>().sprite = ImgMin03; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if (playerAtual == 3)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto01;
            }

            ImgMinUI03.GetComponent<Image>().sprite = ImgMinMorto03;

        }

        if (Player04.GetComponent<Morto>().morto != true)   //se o player não foi destruido 
        {
            if (Player04.GetComponent<Movimento>().atual == 4 && Player04.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual04;
                ImgMinUI04.GetComponent<Image>().sprite = ImgMinSelecionado04;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI04.GetComponent<Image>().sprite = ImgMin04; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if(playerAtual == 4)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto02;
            }
            
            ImgMinUI04.GetComponent<Image>().sprite = ImgMinMorto04;

        }


        if (Player05.GetComponent<Morto>().morto != true)   //se o player não foi destruido 
        {
            if (Player05.GetComponent<Movimento>().atual == 5 && Player04.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual05;
                ImgMinUI05.GetComponent<Image>().sprite = ImgMinSelecionado05;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI05.GetComponent<Image>().sprite = ImgMin05; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if (playerAtual == 5)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto01;
            }

            ImgMinUI05.GetComponent<Image>().sprite = ImgMinMorto05;

        }

        if (Player06.GetComponent<Morto>().morto != true)   //se o player não foi destruido 
        {
            if (Player06.GetComponent<Movimento>().atual == 6 && Player06.GetComponent<Morto>().morto == false)         //se é sua vez e esta vivo
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtual06;
                ImgMinUI06.GetComponent<Image>().sprite = ImgMinSelecionado06;
            }
            else                                                                                                        //se não é sua vez
            {
                ImgMinUI06.GetComponent<Image>().sprite = ImgMin06; ;
            }
        }
        else                                                                                                            //se esta morto
        {
            if (playerAtual == 6)
            {
                ImgAtualUI.GetComponent<Image>().sprite = ImgAtualMorto02;
            }

            ImgMinUI06.GetComponent<Image>().sprite = ImgMinMorto06;

        }






        #endregion //



        //item
        playerAtual = offsetCamera.GetComponent<Troca_Personagem>().personagemAtual;
        //Debug.Log(playerAtual);

        //item Player selecionado
        if (playerAtual == 1)
        {
            if (Player01.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player01.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }
        

        //item player miniaturas
        if (Player01.GetComponent<itemMao>().itemPego!= null)
        {
            item01.SetActive(true);
            item01.GetComponent<Image>().sprite = Player01.GetComponent<itemMao>().itemPego;            
            
        }
        else
        {
            item01.SetActive(false);
        }

        //item Player selecionado
        if (playerAtual == 2)
        {
            if (Player02.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player02.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }


        //item player miniaturas
        if (Player02.GetComponent<itemMao>().itemPego != null)
        {
            item02.SetActive(true);
            item02.GetComponent<Image>().sprite = Player02.GetComponent<itemMao>().itemPego;

        }
        else
        {
            item02.SetActive(false);
        }


        //item Player selecionado
        if (playerAtual == 3)
        {
            if (Player03.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player03.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }


        //item player miniaturas
        if (Player03.GetComponent<itemMao>().itemPego != null)
        {
            item03.SetActive(true);
            item03.GetComponent<Image>().sprite = Player03.GetComponent<itemMao>().itemPego;

        }
        else
        {
            item03.SetActive(false);
        }

        //item Player selecionado
        if (playerAtual == 4)
        {
            if (Player04.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player04.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }


        //item player miniaturas
        if (Player04.GetComponent<itemMao>().itemPego != null)
        {
            item04.SetActive(true);
            item04.GetComponent<Image>().sprite = Player04.GetComponent<itemMao>().itemPego;

        }
        else
        {
            item04.SetActive(false);
        }

        //item Player selecionado
        if (playerAtual == 5)
        {
            if (Player05.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player05.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }


        //item player miniaturas
        if (Player05.GetComponent<itemMao>().itemPego != null)
        {
            item05.SetActive(true);
            item05.GetComponent<Image>().sprite = Player05.GetComponent<itemMao>().itemPego;

        }
        else
        {
            item05.SetActive(false);
        }


        //item Player selecionado
        if (playerAtual == 6)
        {
            if (Player06.GetComponent<itemMao>().itemPego != null)
            {
                itemSelecionado.SetActive(true);
                itemSelecionado.GetComponent<Image>().sprite = Player06.GetComponent<itemMao>().itemPego;
            }
            else
            {
                itemSelecionado.SetActive(false);
            }
        }


        //item player miniaturas
        if (Player06.GetComponent<itemMao>().itemPego != null)
        {
            item06.SetActive(true);
            item06.GetComponent<Image>().sprite = Player06.GetComponent<itemMao>().itemPego;

        }
        else
        {
            item06.SetActive(false);
        }







    }
}
