﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefinirMarcador : MonoBehaviour
{
    [SerializeField] Sprite marcadorAtivo, marcadorInativo;

    void Update()
    {
        if (GetComponentInParent<Movimento>().ativo)
            GetComponentInChildren<Image>().sprite = marcadorAtivo;
        else
            GetComponentInChildren<Image>().sprite = marcadorInativo;
    }
}
