﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Troca_Personagem : MonoBehaviour
{
    public Transform pers1, pers2, pers3, pers4, pers5, pers6;
    private GameObject trocar;
    public GameObject cameraPrincipal;
    public GameObject canvas;

    public int personagemAtual = 1;

    public Transform Alvo;

    void Update()
    {
        //Debug.Log(personagemAtual);

        if (Alvo != null)
        {
            transform.position = Alvo.position;
        }

        if (canvas.GetComponent<Estatua>().estatua == false)
        {

            if (Input.GetKeyDown(KeyCode.Alpha1) && pers1.gameObject.GetComponent<Morto>().morto==false)
            {
                Alvo = pers1;
                personagemAtual = 1;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) && pers2.gameObject.GetComponent<Morto>().morto == false)
            {
                Alvo = pers2;
                personagemAtual = 2;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) && pers3.gameObject.GetComponent<Morto>().morto == false)
            {
                Alvo = pers3;
                personagemAtual = 3;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha4) && pers4.gameObject.GetComponent<Morto>().morto == false)
            {
                Alvo = pers4;
                personagemAtual = 4;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha5) && pers5.gameObject.GetComponent<Morto>().morto == false)
            {
                Alvo = pers5;
                personagemAtual = 5;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha6) && pers6.gameObject.GetComponent<Morto>().morto == false)
            {
                Alvo = pers6;
                personagemAtual = 6;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            /*if (transform.parent != trocar)
            {
                try
                {
                    transform.position = trocar.transform.position;
                    transform.SetParent(trocar.transform);
                }
                catch
                {

                }
            }*/
        }
    }
}
