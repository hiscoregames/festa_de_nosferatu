﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interacao : MonoBehaviour
{
    public KeyCode teclaDeInteracao;
    public delegate void InteractionAction();
    public static event InteractionAction OnInteraction;
    public GameObject canvas;
    

    void Update()
    {
        if (gameObject.GetComponent<Movimento>() != null && gameObject.GetComponent<Movimento>().ativo && canvas.GetComponent<Pause>().pause!=true)
        {
            //Debug.Log("Confere");
            if (Input.GetKeyDown(teclaDeInteracao))
            {
                //Debug.Log("Interagiu");
                OnInteraction();
            }
        }
    }
}
