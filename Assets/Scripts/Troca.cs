﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Troca : MonoBehaviour
{
    public GameObject pers1, pers2, pers3, pers4, pers5, pers6;
    private GameObject trocar;
    public GameObject cameraPrincipal;
    public GameObject canvas;

    void Update()
    {
        if (canvas.GetComponent<Estatua>().estatua == false)
        {

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                trocar = pers1;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                trocar = pers2;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                trocar = pers3;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                trocar = pers4;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                trocar = pers5;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                trocar = pers6;
                cameraPrincipal.GetComponent<ModoCamera>().DesligaCamera();
            }
            if (transform.parent != trocar)
            {
                try
                {
                    transform.position = trocar.transform.position;
                    transform.SetParent(trocar.transform);
                }
                catch
                {

                }
            }
        }
    }
}
