﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimento : MonoBehaviour
{
    private Rigidbody rb;
    private float speed;
    public float VelAndando;
    public float VelPerseguicao;

    private float hori, verti;

    public bool ativo = false;
    public int personagem;
    public int atual = 1;

    public bool perseguido=false;

    public GameObject canvas;
    public GameObject balao;

    private Animator anim;

    public GameObject Player01, Player02, Player03, Player04, Player05, Player06;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        speed = VelAndando;
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //Debug.Log("Perosnagem: " + atual);
        //Debug.Log(anim.GetInteger(2));
        if (canvas.GetComponent<Estatua>().estatua == false && GetComponent<Morto>().morto==false)
        {
            hori = Input.GetAxisRaw("Horizontal");
            verti = Input.GetAxisRaw("Vertical");
        }

        if (hori != 0 || verti != 0)
        {
            if (perseguido == false)
            {
                anim.SetInteger("Move", 1);
            }
            else
            {
                anim.SetInteger("Move", 2);
            }


            
        }
        else
        {
            anim.SetInteger("Move", 0);
        }
        

        if(hori == 1 && ativo==true)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * 5);
        }

        if (hori == -1 && ativo == true)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, -90, 0), Time.deltaTime * 5);
        }

        if (verti == 1 && ativo == true)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 5);
        }

        if (verti == -1 && ativo == true)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, -180, 0), Time.deltaTime * 5);
        }



        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (Player01.GetComponent<Morto>().morto== false)
            {
                atual = 1;
            }
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (Player02.GetComponent<Morto>().morto == false)
            {
                atual = 2;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (Player03.GetComponent<Morto>().morto == false)
            {
                atual = 3;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (Player04.GetComponent<Morto>().morto == false)
            {
                atual = 4;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (Player05.GetComponent<Morto>().morto == false)
            {
                atual = 5;
            }
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            if (Player06.GetComponent<Morto>().morto == false)
            {
                atual = 6;
            }
        }
        if (atual == personagem)
        {
            ativo = true;
            anim.SetBool("Ativo", true);
        }
        else
        {
            ativo = false;
            anim.SetBool("Ativo", false);
        }


        if (perseguido == true)
        {
            speed = VelPerseguicao;
            balao.SetActive(true);
            balao.GetComponent<Persamento>().Pensar("Perseguindo0" + personagem.ToString());
        }
    }

    private void FixedUpdate()
    {
        if (canvas.GetComponent<Estatua>().estatua == false && GetComponent<Morto>().morto==false )
        {
            if (ativo) rb.velocity = new Vector3(hori * speed, rb.velocity.y, verti * speed);
            anim.SetInteger("Move", 1);
        }
        //if (ativo) rb.velocity = new Vector3(hori * speed, rb.velocity.z, verti * speed);
    }

    public void desacelarear()
    {
        StartCoroutine(SeAcalma());
    }


    IEnumerator SeAcalma()
    {

        //yield on a new YieldInstruction that waits for 5 seconds.
        perseguido = false;
        yield return new WaitForSeconds(5);
        speed = VelAndando;


    }
}
