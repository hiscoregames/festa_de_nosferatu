﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gerente_CurtScene : MonoBehaviour
{
    public GameObject canvas;
    public int secsEsperar;

   public void proximo()
    {
        StartCoroutine(espera());
    }

    IEnumerator espera()
    {
        yield return new WaitForSeconds(secsEsperar);
        canvas.GetComponent<Curtscene_Texto>().Proximo();

    }
}
