﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estante : MonoBehaviour
{
    public Animator anim;
    public bool TemLivro;
    public bool pegaLivro=false;
    public Sprite LivroConteudo;
    public GameObject modoTelaCheia;
    //private GameObject playerProximo;
    public GameObject LivroEstante;
    public GameObject som;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pegaLivro == false)
        {
            LivroEstante.GetComponent<BoxCollider>().enabled = false;
        }
    }

    public void EstanteLivros()
    {
        if (TemLivro == true)
        {
            //LivroEstante.transform.SetParent(null);
            StartCoroutine(Livro());
            
        }
    }

    public void LiberaPassagem()
    {
        anim.SetBool("Arrastar_X_Esquerda", true);
        som.GetComponent<Gerente_Som>().TocaSom(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (TemLivro == true)
            {
                other.gameObject.GetComponent<Marcador_Player>().interacao = true;
            }
            
            Interacao.OnInteraction += EstanteLivros;

        }

       
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {

            other.gameObject.GetComponent<Marcador_Player>().interacao = false;
            Interacao.OnInteraction -= EstanteLivros;

        }
        

    }

    IEnumerator Livro()
    {


        if (modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia == false)
        {
            anim.SetBool("Interage_Livro", true);

            if (pegaLivro == false)
            {
                yield return new WaitForSeconds(2f);
                modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().VerImagem(LivroConteudo);
            }
            
            
            anim.SetBool("Interage_Livro", false);
        }


        }
    }
