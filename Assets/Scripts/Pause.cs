﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pauseImagem;
    public GameObject canvas;
    public bool pause = false;
    public GameObject camera;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Botão de pause");
            if (pause == false)
            {
                Debug.Log("Pausado");
                PauseOn();
            }

            else if(pause == true)
            {
                Debug.Log("Desausado");
                PauseOff();
            }
        }
        
    }

    public void PauseOn()
    {
        pause = true;
        pauseImagem.SetActive(true);
        canvas.GetComponent<Estatua>().estatua = true;

        if(camera.GetComponent<ModoCamera>().modoCamera==true)
        {
            camera.GetComponent<ModoCamera>().DesligaCamera();
        }
    }

    public void PauseOff()
    {
        pause = false;
        pauseImagem.SetActive(false);
        canvas.GetComponent<Estatua>().estatua = false;
    }
}
