﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderCadeira : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (gameObject.GetComponentInChildren<CadeiraJantar>() != null)
            {
                gameObject.GetComponentInChildren<CadeiraJantar>().interactable = true;
                //Debug.Log("Player entrou");
                other.gameObject.GetComponent<Marcador_Player>().interacao = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (gameObject.GetComponentInChildren<CadeiraJantar>() != null)
            {
                gameObject.GetComponentInChildren<CadeiraJantar>().interactable = false;
                //Debug.Log("Player saiu");
                other.gameObject.GetComponent<Marcador_Player>().interacao = false;
            }
        }
    }
}
