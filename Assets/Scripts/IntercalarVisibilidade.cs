﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntercalarVisibilidade : MonoBehaviour
{
    public GameObject trigger;
    private MeshRenderer mesh;
    public bool IniciaRevelado;
    
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        if (IniciaRevelado == false)
        {
            mesh.enabled = false;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger.GetComponent<Trigger_Visibilidades>().revelado == true)
        {
            Intercalar();
        }
    }



    public void Intercalar()
    {
        if (IniciaRevelado == true)
        {
            mesh.enabled = false;
        }
        else
        {
            mesh.enabled = true;
        }
    }
}
