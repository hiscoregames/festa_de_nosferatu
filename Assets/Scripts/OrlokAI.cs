﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class OrlokAI : MonoBehaviour
{
    enum State { Patrol, Chase, Bite, Amanhecer }; //estados existentes
    State currentState, nextState; //váriaveis para controlar os estados
    [SerializeField] Transform[] patrolWaypoints; //array dos waypoints
    Transform currentWaypoint; //waypoint atual da patrulha
    NavMeshAgent nav; //variável para guardar o NavMeshAgent
    [SerializeField] float patrolWaypointRange; //distância até o waypoint para mudar de waypoint
    int currentIndex; //índex atual do array dos waypoints
    [SerializeField] float radius; //alcance da "visão"
    [SerializeField] float angulo; //ângulo da "visão"
    GameObject chasePlayer; //player sendo perseguido
    LayerMask layerMask; //layer para o linecast
    [SerializeField] int segundos; //segundos para parar de seguir o jogador
    Coroutine chasingRoutine; //coroutine para o tempo de seguir
    public delegate void BiteAction();
    public static event BiteAction OnBit;
    [SerializeField] float biteRange; //distância para ele morder
    bool losingPlayer; //variável para controlar 

    public float VelPatrulha;
    public float VelPerseguicao;

    public GameObject alvo;
    //public AudioClip somViuPlayer;
    public GameObject som;

  


    private Animator anim;

    //void OnEnable() //isso era o q eu tinha preparado pra quando tivesse as animações
    //{
    //    Tempo.OnAmanhecer += Amanhecer;
    //    Tempo.OnAnoitecer += Anoitecer;
    //}

    //void OnDisable()
    //{
    //    Tempo.OnAmanhecer -= Amanhecer;
    //    Tempo.OnAnoitecer -= Anoitecer;
    //}

    //void Amanhecer()
    //{

    //}

    //void Anoitecer()
    //{

    //}

    void Start()
    {
        nav = GetComponent<NavMeshAgent>(); //guarda o NavMeshAgent
        currentState = State.Patrol; //bota para patrulhar
        layerMask = LayerMask.GetMask("Parede"); //complicado entender isso aqui
        currentWaypoint = patrolWaypoints[Random.Range(0, patrolWaypoints.Length)]; //pega um waypoint aleatório da lista dos waypoints
        anim = GetComponent<Animator>();
        //audio = GetComponent<AudioSource>();
    }

    void Update()
    {
        switch (currentState)
        {
            case State.Patrol:
                Patrol();
                PatrolDecision();
                break;
            case State.Chase:
                Chase();
                ChaseDecision();
                break;
            //case State.Bite:
            //    Bite();
            //    break;
        }
        if (nextState != currentState)
            currentState = nextState;
        //print(currentState);
    }

    void Patrol() //o que fazer durante a patrulha
    {
        if (alvo != null)
        {
            alvo.GetComponent<Movimento>().desacelarear();
            alvo = null;
        }

        anim.SetInteger("Animacao", 1);
        nav.speed = VelPatrulha;
        nav.enabled = true; //liga o NavMehAgent
        if (nav.remainingDistance < patrolWaypointRange) //se a distância até o waypoint atual for menor q a variável
            currentWaypoint = patrolWaypoints[Random.Range(0, patrolWaypoints.Length)]; //pega um novo waypoint aleatório e grava nessa variável
        nav.SetDestination(currentWaypoint.position); //bota o novo waypoint como destino
    }

    void PatrolDecision() //lógica para sair da patrulha
    {
        chasePlayer = null;
        GameObject closestPlayer = null;
        float distancia = 0; //distância até o collider do player
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius); //faz uma esfera ao redor e grava os colliders q são atingidos
        foreach (Collider collider in hitColliders) //percorre todos os colliders
        {
            if (collider.CompareTag("Player")) //se o collider for do player
            {
                Vector3 targetDir = collider.transform.position - transform.position; //distância até o collider
                float angle = Vector3.Angle(targetDir, transform.forward); //ângulo entre onde o Orlok tá olhando e o collider
                if (angle <= angulo && !Physics.Linecast(transform.position, collider.transform.position, layerMask)) //se o ângulo for menor q a variável de controle e não tiver nada tampando a visão do Orlok
                {
                    //print("Achou player");

                    som.GetComponent<Gerente_Som>().TocaSom(0);

                    alvo = collider.gameObject;
                    alvo.GetComponent<Movimento>().perseguido = true;

                    anim.SetInteger("Animacao", 2);

                    StartCoroutine(Perseguir());
                    
                    if (distancia == 0) //se a distância for zero (sempre é na primeira percorrida)
                    {
                        distancia = Vector3.Distance(transform.position, collider.transform.position); //guarda a distância até o player do collider sendo percorrido no momento
                        closestPlayer = collider.gameObject; //guarda qual é o game object desse collider
                    }
                    else
                    {
                        if (Vector3.Distance(transform.position, collider.transform.position) < distancia) //se o próximo player estiver mais perto q o anterior
                        {
                            distancia = Vector3.Distance(transform.position, collider.transform.position); //guarda essa distância
                            closestPlayer = collider.gameObject; //bota esse player como o alvo
                        }
                    }
                }
            }
            if (closestPlayer != null) //se tiver um player mais perto q o atual
                chasePlayer = closestPlayer; //bota ele como o novo alvo
        }
        if (chasePlayer != null) //se tem um alvo(player) para perseguir
            nextState = State.Chase; //bota pra mudar pro estado de Chase
    }

    void Chase() //o q fazer durante o chase
    {
        nav.enabled = true; //liga o NavMeshAgent
        if (chasePlayer != null) //se tem um player para perseguir
            nav.SetDestination(chasePlayer.transform.position); //bota ele como o alvo
        else
            nextState = State.Patrol; //bota pra voltar para a patrulha
    }

    void ChaseDecision() //lógica para desistir de perseguir
    {
        if (chasePlayer != null) //se tem um player para perseguir
        {
            if (Physics.Linecast(transform.position, chasePlayer.transform.position, layerMask)) //se ele n tá vendo o player
            {
                if (!losingPlayer) //se for false
                    chasingRoutine = StartCoroutine(ChasingRoutine()); //começa a rotina de perder o player
            }
            else
            if (losingPlayer) //se for true
            {
                StopCoroutine(chasingRoutine); //para a rotina de perder o player
                losingPlayer = false; //bota false

                
            }
            if (nav.remainingDistance < biteRange) //se estiver na distância para morder
                OnBit();
        }
        else
            nextState = State.Patrol; //bota pra voltar pro modo de patrulha
    }

    IEnumerator ChasingRoutine()
    {
        losingPlayer = true; //bota true
       
        //print("Perdeu o player");
        yield return new WaitForSeconds(segundos); //espera os segundos
        nextState = State.Patrol; //bota pra voltar pro modo de patrulha
        chasePlayer = null; //esvazia a variável do player perseguido
        //print("Voltou a patrulhar");
        losingPlayer = false; //bota false
    }


    IEnumerator Perseguir()
    {
        
        yield return new WaitForSeconds(5);
        nav.speed = VelPerseguicao;
    }

    public void PegouPlayer()
    {
        alvo = null;
        StartCoroutine(IPegouPlayer());
        
    }

    IEnumerator IPegouPlayer()
    {
        anim.SetInteger("Animacao", 0);
        yield return new WaitForSeconds(3);
        //ChaseDecision();
        //StartCoroutine(ChasingRoutine());
        currentState = State.Patrol;
    }

    //void Bite()
    //{
    //    nav.enabled = false;
    //}
}
