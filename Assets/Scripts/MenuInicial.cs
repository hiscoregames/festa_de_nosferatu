﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{
    //[SerializeField] Slider musicSlider, efxSlider;

    void Update()
    {
       // if (SoundManager.instance != null)
         //   SoundManager.instance.ChangeVolume(efxSlider.value, musicSlider.value);
    }

    public void Iniciar()
    {
        //Loading.LoadScene("Castelo");
        Loading.LoadScene("Convite");
    }

    public void JogarNovamente()
    {
        Loading.LoadScene("Castelo");
    }

    public void Configuracoes()
    {
        SceneManager.LoadScene("Configuracoes");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void Sair()
    {
        Application.Quit();
    }

    public void VoltarConfiguracoes()
    {
        SceneManager.LoadScene("Menu_Inicial");
    }

    public void VoltarCreditos()
    {
        SceneManager.LoadScene("Menu_Inicial");
    }
}
