﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gerente_Som : MonoBehaviour
{
    public AudioClip[] sons;
    public AudioSource audioObj;


    // Start is called before the first frame update
    void Start()
    {
        //audioObj.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TocaSom(int numero)
    {
        audioObj.clip = sons[numero];
        audioObj.Play();
        
    }
}
