﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chave_Control : MonoBehaviour
{
    public int NChave;
    public Sprite Imagem;
    public GameObject modoTelaCheia;
    public bool pego=false;

    public Sprite Miniatura;
    public GameObject som;
    
    // Start is called before the first frame update
   
    public void itemPego()
    {
        if (Imagem != null)
        { 
            modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().VerImagem(Imagem);
            som.GetComponent<Gerente_Som>().TocaSom(0);
        }
    }


}
