﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tempo : MonoBehaviour
{
    Text text;
    [SerializeField] int duracaoDiaSegundos, duracaoNoiteSegundos;
    int tempoRestante;
    public delegate void AnoitecerAction();
    public static event AnoitecerAction OnAnoitecer;
    public delegate void AmanhecerAction();
    public static event AmanhecerAction OnAmanhecer;

    public bool TempoNoite;

    public GameObject trilaCenario;

    public GameObject pause;


    void Start()
    {
        text = GetComponent<Text>();
        StartCoroutine(Noite());
    }

    void Update()
    {
        if (tempoRestante >= 60)
            text.text = tempoRestante / 60 + ":" + tempoRestante % 60;
        else
            text.text = tempoRestante.ToString();
    }

    public void musica(string periodo)
    {
        if (periodo == "Dia")
        {
            trilaCenario.GetComponent<Troca_Musica>().somDia();
        }
        else if (periodo == "Noite")
        {
            trilaCenario.GetComponent<Troca_Musica>().somNoite();
        }
    }

    IEnumerator Dia()
    {
        TempoNoite = false;
        musica("Dia");

        tempoRestante = duracaoDiaSegundos;
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (pause.GetComponent<Pause>().pause == false)
            {
                tempoRestante--;
            }
            
            if (tempoRestante <= 0)
            {
                //OnAnoitecer();
                StartCoroutine(Noite());
                yield break;
            }
        }
    }

    IEnumerator Noite()
    {
        TempoNoite = true;
        musica("Noite");
        tempoRestante = duracaoNoiteSegundos;
        while (true)
        {
            yield return new WaitForSeconds(1);

            if (pause.GetComponent<Pause>().pause == false)
            {
                tempoRestante--;
            }

            if (tempoRestante <= 0)
            {
                //OnAmanhecer();
                StartCoroutine(Dia());
                yield break;
            }
        }
    }
}
