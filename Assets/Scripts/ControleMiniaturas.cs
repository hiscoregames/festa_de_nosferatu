﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControleMiniaturas : MonoBehaviour
{

    public GameObject Personagem01;
    public GameObject Personagem02;
    public GameObject Personagem03;
    public GameObject Personagem04;
    public GameObject Personagem05;
    public GameObject Personagem06;

    public GameObject ImagemPlayer01;
    public GameObject ImagemPlayer02;
    public GameObject ImagemPlayer03;
    public GameObject ImagemPlayer04;
    public GameObject ImagemPlayer05;
    public GameObject ImagemPlayer06;

    public GameObject ImagemSelecionadoMorto;

    public GameObject ImagemMinPlayer01;
    public GameObject ImagemMinPlayer02;
    public GameObject ImagemMinPlayer03;
    public GameObject ImagemMinPlayer04;
    public GameObject ImagemMinPlayer05;
    public GameObject ImagemMinPlayer06;

    public GameObject ImagemMortoPlayer01;
    public GameObject ImagemMortoPlayer02;
    public GameObject ImagemMortoPlayer03;
    public GameObject ImagemMortoPlayer04;
    public GameObject ImagemMortoPlayer05;
    public GameObject ImagemMortoPlayer06;

    public GameObject ImagemPlayerAtual01, ImagemPlayerAtual02, ImagemPlayerAtual03, ImagemPlayerAtual04, ImagemPlayerAtual05, ImagemPlayerAtual06;

    // Start is called before the first frame update
    private void Start()
    {
        ImagemSelecionadoMorto.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        //Selecionar Miniatura principal

        if (Personagem01 != null && Personagem01.GetComponent<Movimento>() != null && Personagem01.GetComponent<Morto>() != null)
        {
            if (Personagem01.GetComponent<Movimento>().atual == 1 && Personagem01.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(true);
                ImagemPlayer02.SetActive(false);
                ImagemPlayer03.SetActive(false);
                ImagemPlayer04.SetActive(false);
                ImagemPlayer05.SetActive(false);
                ImagemPlayer06.SetActive(false);
                ImagemSelecionadoMorto.SetActive(false);
            }
        }
        else
        {
            ImagemPlayer01.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer01.SetActive(true);
            ImagemMinPlayer01.SetActive(false);

        }

        if (Personagem02 != null && Personagem02.GetComponent<Movimento>() != null && Personagem02.GetComponent<Morto>() != null)
        {

            if (Personagem02.GetComponent<Movimento>().atual == 2 && Personagem02.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(false);
                ImagemPlayer02.SetActive(true);
                ImagemPlayer03.SetActive(false);
                ImagemPlayer04.SetActive(false);
                ImagemPlayer05.SetActive(false);
                ImagemPlayer06.SetActive(false);
                ImagemSelecionadoMorto.SetActive(false);
            }

        }
        else
        {
            ImagemPlayer02.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer02.SetActive(true);
            ImagemMinPlayer02.SetActive(false);

        }

        if (Personagem03 != null && Personagem03.GetComponent<Movimento>() != null && Personagem03.GetComponent<Morto>() != null)
        {

            if (Personagem03.GetComponent<Movimento>().atual == 3 && Personagem03.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(false);
                ImagemPlayer02.SetActive(false);
                ImagemPlayer03.SetActive(true);
                ImagemPlayer04.SetActive(false);
                ImagemPlayer05.SetActive(false);
                ImagemPlayer06.SetActive(false);
                ImagemSelecionadoMorto.SetActive(false);
            }

        }
        else
        {
            ImagemPlayer03.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer03.SetActive(true);
            ImagemMinPlayer03.SetActive(false);

        }


        if (Personagem04 != null && Personagem04.GetComponent<Movimento>() != null && Personagem04.GetComponent<Morto>() != null)
        {
            if (Personagem04.GetComponent<Movimento>().atual == 4 && Personagem04.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(false);
                ImagemPlayer02.SetActive(false);
                ImagemPlayer03.SetActive(false);
                ImagemPlayer04.SetActive(true);
                ImagemPlayer05.SetActive(false);
                ImagemPlayer06.SetActive(false);
                ImagemSelecionadoMorto.SetActive(false);
            }

        }
        else
        {
            ImagemPlayer04.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer04.SetActive(true);
            ImagemMinPlayer04.SetActive(false);

        }

        if (Personagem05 != null && Personagem05.GetComponent<Movimento>() != null && Personagem05.GetComponent<Morto>() != null)
        {

            if (Personagem05.GetComponent<Movimento>().atual == 5 && Personagem05.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(false);
                ImagemPlayer02.SetActive(false);
                ImagemPlayer03.SetActive(false);
                ImagemPlayer04.SetActive(false);
                ImagemPlayer05.SetActive(true);
                ImagemPlayer06.SetActive(false);
                ImagemSelecionadoMorto.SetActive(false);
            }

        }
        else
        {
            ImagemPlayer05.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer05.SetActive(true);
            ImagemMinPlayer05.SetActive(false);

        }

        if (Personagem06 != null && Personagem06.GetComponent<Movimento>() != null && Personagem06.GetComponent<Morto>() != null)
        {

            if (Personagem06.GetComponent<Movimento>().atual == 6 && Personagem06.GetComponent<Morto>().morto == false)
            {
                ImagemPlayer01.SetActive(false);
                ImagemPlayer02.SetActive(false);
                ImagemPlayer03.SetActive(false);
                ImagemPlayer04.SetActive(false);
                ImagemPlayer05.SetActive(false);
                ImagemPlayer06.SetActive(true);
                ImagemSelecionadoMorto.SetActive(false);
            }

        }
        else
        {
            ImagemPlayer06.SetActive(false);
            ImagemSelecionadoMorto.SetActive(true);
            ImagemMortoPlayer06.SetActive(true);
            ImagemMinPlayer06.SetActive(false);

        }


    }

}




