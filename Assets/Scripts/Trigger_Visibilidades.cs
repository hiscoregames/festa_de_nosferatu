﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Visibilidades : MonoBehaviour
{
    public bool revelado;
    public bool PlayerIsTrigger;
    
    
    public void reveleSeusSecredos()
    {
        revelado = true;
    }

    public void oculte()
    {
        revelado = false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && PlayerIsTrigger==true)
        {
            reveleSeusSecredos();
        }
    }

}
