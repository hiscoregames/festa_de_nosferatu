﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ocultar : MonoBehaviour
{
    public GameObject trigger;
    private MeshRenderer mesh;
    // Start is called before the first frame update
    public void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger.GetComponent<Trigger_Visibilidades>().revelado == false)
        {
            mesh.enabled=false;
        }
        else
        {
            mesh.enabled = true;
        }

    }
}
