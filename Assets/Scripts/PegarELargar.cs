﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PegarELargar : MonoBehaviour
{
    public GameObject hand;
    public GameObject itemperto;
    //public KeyCode teclaDeInteracao;
    public GameObject modoTelaCheia;
    public GameObject canvas;
    public BoxCollider[] box;

    public bool PodeInteragir=true;

    public Sprite avisoMaoCheia;

   

    private void Update()
    {
        #region Condigo Henry
        //pegar
        /*if (Input.GetKeyDown(KeyCode.J) && hand.transform.childCount == 0 && itemperto != null)
        {
            itemperto.transform.position = hand.transform.position;
            itemperto.GetComponent<BoxCollider>().enabled = false;
            itemperto.GetComponent<Rigidbody>().isKinematic = true;
            itemperto.transform.SetParent(hand.transform);
            itemperto = null;
        }

        //largar
        if (Input.GetKeyDown(KeyCode.K) && hand.transform.childCount != 0)
        {
            itemperto = hand.transform.GetChild(0).gameObject;
            itemperto.GetComponent<BoxCollider>().enabled = true;
            itemperto.GetComponent<Rigidbody>().isKinematic = false;
            hand.transform.GetChild(0).SetParent(null);
        }
       */
        #endregion Condigo Henry

        if (modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia == false)
        {
            StartCoroutine(Espera());
        }

        //Debug.Log(itemperto);
        //Debug.Log(itemperto.name);

    }

    public void PegarLargar()
    {

        //soltar
        if (hand.transform.childCount != 0 && modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia == false && PodeInteragir == true 
            && GetComponent<Movimento>().ativo==true && GetComponent<Marcador_Player>().interacao==false)
        {
            PodeInteragir = false;
            //Debug.Log("Solta");
            StartCoroutine(SoltaObjeto());
            GetComponent<itemMao>().Largou();
            //GetComponent<itemMao>().itemNaMao = "Vazia";


        }
        //pegar mão cheia
        /*else if (hand.transform.childCount != 0 && modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().ModotelaCheia == false && PodeInteragir == true
            && GetComponent<Movimento>().ativo == true && GetComponent<Marcador_Player>().interacao == true)
             {

               modoTelaCheia.GetComponent<Objeto_Tela_Cheia>().VerImagem(avisoMaoCheia);


             }*/

        //pegar
        else if (hand.transform.childCount == 0 && itemperto != null && PodeInteragir == true && GetComponent<Movimento>().ativo == true)
        {
            gameObject.GetComponent<Marcador_Player>().interacao = false;
            PodeInteragir = false;

            if(itemperto.transform.parent == true)
            {
                //Debug.Log(itemperto.transform.parent);
                if (itemperto.transform.parent.gameObject.GetComponent<itemMao>())
                {
                    
                    itemperto.transform.parent.gameObject.GetComponent<itemMao>().Largou();
                }
            }

            //Debug.Log("pega");
            GetComponent<itemMao>().Definir(itemperto.GetComponent<Chave_Control>().Miniatura);
            //GetComponent<itemMao>().itemNaMao = itemperto.name.ToString();
            itemperto.GetComponent<Chave_Control>().itemPego();
            //hand.GetComponent<Hand>().maoCheia = true;
            itemperto.transform.position = hand.transform.position;

            box = itemperto.GetComponents<BoxCollider>();
            box[0].enabled = false;
            box[1].enabled = false;


            itemperto.GetComponent<Rigidbody>().isKinematic = true;
            itemperto.transform.SetParent(hand.transform);
            itemperto = null;

            //StartCoroutine(Espera());


        }
       


    }

   
    IEnumerator SoltaObjeto()
    {
        itemperto = hand.transform.GetChild(0).gameObject;

        
        itemperto.GetComponent<Rigidbody>().isKinematic = false;
        hand.transform.GetChild(0).SetParent(null);
        //hand.GetComponent<Hand>().maoCheia = false;
        yield return new WaitForSeconds(0.2f);
        box = itemperto.GetComponents<BoxCollider>();
        box[0].enabled = true;
        box[1].enabled = true;
        box = null;
        itemperto = null;
        //StartCoroutine(Espera());
        
    }

    IEnumerator Espera()
    {
        
        yield return new WaitForSeconds(0.5f);
        PodeInteragir = true;
    }




    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item") || other.CompareTag("Chave"))
        {
            itemperto = other.gameObject;
            Interacao.OnInteraction += PegarLargar;
            gameObject.GetComponent<Marcador_Player>().interacao = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Item") || other.CompareTag("Chave"))
        {

            itemperto = null;
            Interacao.OnInteraction -= PegarLargar;
            gameObject.GetComponent<Marcador_Player>().interacao = false;
        }
    }

}
