﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Destroi_Trigger : MonoBehaviour
{
    public int cont = 0;
    public GameObject som;

    public Sprite[] Nota;
    //public Sprite Nota00, Nota01, Nota02, Nota03, Nota04, Nota05, Nota06;

    public GameObject NotaUI;

    public GameObject Orlok;

    public GameObject[] Players;



    // Start is called before the first frame update
    void Start()
    {
        //NotaUI.GetComponent<Image>().sprite = Nota00;
        checarPlayervivos();
    }

    // Update is called once per frame
    void Update()
    {
        if (cont == 1)
        {
            Orlok.SetActive(true);
        }



    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        if (other.tag == "Player")
        {
            cont++;
            //NotaUI.GetComponent<Image>().color=Color.white;
            NotaUI.GetComponent<Image>().sprite = Nota[cont];
            som.GetComponent<Gerente_Som>().TocaSom(0);



        }

        Destroy(other.gameObject);
    }

    public void checarPlayervivos()
    {
        /*for (int i = 0; i < Players.Length; i++)
        {
            if (PlayerPrefs.GetInt("Player0" +i+1.ToString()) == 0)
            {
                Destroy(Players[i]);
            }
        }
    }*/

        if (PlayerPrefs.GetInt("Player01") == 0)
        {
            Destroy(Players[0]);
        }
        if (PlayerPrefs.GetInt("Player02") == 0)
        {
            Destroy(Players[1]);
        }
        if (PlayerPrefs.GetInt("Player03") == 0)
        {
            Destroy(Players[2]);
        }
        if (PlayerPrefs.GetInt("Player04") == 0)
        {
            Destroy(Players[3]);
        }
        if (PlayerPrefs.GetInt("Player05") == 0)
        {
            Destroy(Players[4]);
        }
        if (PlayerPrefs.GetInt("Player06") == 0)
        {
            Destroy(Players[5]);
        }
    }
}
