﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Orlok_Vitoria : MonoBehaviour
{
    private NavMeshAgent nav;
    public Transform alvo;
    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        nav=GetComponent<NavMeshAgent>();
        anim=GetComponent<Animator>();
        nav.SetDestination(alvo.position);
    }

    // Update is called once per frame
    void Update()
    {
        


        if (nav.remainingDistance <= 0.5)
        {
            nav.GetComponent<NavMeshAgent>().enabled = false;
            anim.SetBool("Chegou", true);
        }
           

        

    }
}
